<?php

namespace Qstione\Pipeline;

abstract class PipelineAbstract implements PipelineInterface
{
    /** @var PipelineInterface */
    private $next;

    /** @var \stdClass */
    private $dataContext;

    public function __construct()
    {
        $this->dataContext = new \stdClass();
    }

    /**
     * @return PipelineInterface
     */
    public function getNext(): ?PipelineInterface
    {
        return $this->next;
    }

    /**
     * @param PipelineInterface|null $next
     * @return $this
     */
    public function addNext(?PipelineInterface $next): PipelineInterface
    {
        if ($this->hasNext()) {
            $this->getNext()->addNext($next);

            return $this;
        }

        $this->next = $next;

        return $this;
    }

    /**
     * @return bool
     */
    public function hasNext(): bool
    {
        return !empty($this->getNext());
    }

    /**
     * @param string $key
     * @param $data
     * @return PipelineInterface
     */
    public function setField(string $key, $data): PipelineInterface
    {
        if (is_array($data)) {
            return $this->setFields($key, $data);
        }

        $this->getDataContext()->$key = $data;

        return $this;
    }

    /**
     * @param string $key
     * @param array $data
     * @return PipelineInterface
     */
    public function setFields(string $key, array $data): PipelineInterface
    {
        $subcontext = new \stdClass();

        array_map(function ($subkey, $value) use ($subcontext, $key) {
            $subcontext->$subkey = $value;
        }, array_keys($data), $data);

        $this->getDataContext()->$key = $subcontext;

        return $this;
    }

    /**
     * @param string $field
     * @return mixed
     */
    public function getField(string $field)
    {
        return $this->getDataContext()->$field;
    }

    /**
     * @return \stdClass
     */
    public function getDataContext(): \stdClass
    {
        if ($this->hasNext()) {
            return $this->getNext()->getDataContext();
        }

        return $this->dataContext;
    }

    public function handle()
    {
        $this->run();

        if ($this->hasNext()) {
            $this->getNext()->handle();
        }
    }

    abstract public function run();
}
