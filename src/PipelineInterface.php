<?php

namespace Qstione\Pipeline;

interface PipelineInterface
{
    public function __construct();

    public function getField(string $field);
    public function setField(string $key, $data);
    public function setFields(string $key, array $data);

    public function getNext();
    public function addNext(self $next): self;
    public function hasNext();

    public function getDataContext(): \stdClass;

    public function handle();
    public function run();
}
